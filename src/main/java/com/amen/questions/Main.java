package com.amen.questions;

import com.amen.questions.questionnaire.Question;

import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Questionnaire questionnaire = new Questionnaire();
        Scanner scanner = new Scanner(System.in);

        while(questionnaire.hasQuestions()){
            Question question = questionnaire.getNext();

            print(question);

            scanner.next();
        }
    }

    private static void print(Question question){
        System.out.println(question.getId() + " :>" + question.getQuestion());
        for (Map.Entry<String, String> entry :
             question.getAnswers().entrySet()) {
            System.out.println(entry.getKey() + "==> " + entry.getValue());
        }
    }
}
