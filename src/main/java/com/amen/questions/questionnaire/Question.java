package com.amen.questions.questionnaire;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Question {
    private int id;
    private QuestionType type;
    private int nextQuestionId;
    private String showCondition;
    private String question;
    private Map<String, String> answers;

    private ShuffledQuestionSet shuffledQuestionSet;

    /*
    "0": {
        "question": "Płeć?",
        "questionType": "SIMPLE_QUESTION",
        "answers":{
          "a": "Kobieta",
          "b": "Mężczyzna",
          "c": "Domyśl się",
          "d": "Jest więcej płci..."
        },
        "next": "1",
        "condition":""
      }
     */


    public Question(int id, JsonObject jsonObject) throws IllegalArgumentException {
        this(id, Integer.parseInt(jsonObject.get("next").getAsString()), jsonObject);
    }

    public Question(int id, int nextQuestionId, JsonObject jsonObject) throws IllegalArgumentException {
        this.id = id;
        this.type = QuestionType.valueOf(jsonObject.get("questionType").getAsString());
        this.nextQuestionId = nextQuestionId;
        this.showCondition = jsonObject.get("condition").getAsString();

        if (type == QuestionType.SIMPLE_QUESTION) {
            // load answers
            JsonObject answersElement = jsonObject.getAsJsonObject("answers");
            Set<Map.Entry<String, JsonElement>> answersSet = answersElement.entrySet();

            answers = new HashMap<>();

            // load answers from set (json) to map
            for (Map.Entry<String, JsonElement> entry : answersSet) {
                answers.put(entry.getKey(), entry.getValue().getAsString());
            }

            // load question
            this.question = jsonObject.get("question").getAsString();
        } else {
            shuffledQuestionSet = ShuffleQuestionsContainer.getInstance().get(jsonObject.get("setId").getAsInt());
        }
    }

    public Question(JsonObject jsonObject) throws IllegalArgumentException {
        this(Integer.parseInt(jsonObject.get("id").getAsString()), -1,jsonObject);
    }

    /**
     * If shuffled
     *
     * @return
     */
    public boolean hasNext() {
        return shuffledQuestionSet != null && shuffledQuestionSet.hasMoreQuestions();
    }

    public Question getNext() {
        return shuffledQuestionSet.shuffleNext();
    }

    public boolean isSimple() {
        return type == QuestionType.SIMPLE_QUESTION;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Map<String, String> getAnswers() {
        return answers;
    }

    public void setAnswers(Map<String, String> answers) {
        this.answers = answers;
    }
}
