package com.amen.questions.questionnaire;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public abstract class AbstractJsonReader {
    protected String fileName;

    public AbstractJsonReader(String fileName) {
        this.fileName = fileName;
    }

    protected String readFileContentIntoString() {
        StringBuilder builder = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // log out
//        Logger.getLogger(getClass().getName()).log(Level.INFO, "Parsed: " + builder.toString());

        return builder.toString();
    }

    protected JsonObject parseString(String content) {
        Gson gson = new Gson();

        return gson.fromJson(content, JsonElement.class).getAsJsonObject();
    }
}
