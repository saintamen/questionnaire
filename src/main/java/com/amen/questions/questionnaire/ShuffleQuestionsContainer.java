package com.amen.questions.questionnaire;

import java.util.HashMap;
import java.util.Map;

public class ShuffleQuestionsContainer {
    // singletone instance
    private static ShuffleQuestionsContainer instance = new ShuffleQuestionsContainer();

    // private field containing sets
    private Map<Integer, ShuffledQuestionSet> questionSets;

    // private constructor
    private ShuffleQuestionsContainer(){
        loadShuffledQuestions();
    }

    // static getter for instance
    public static ShuffleQuestionsContainer getInstance(){
        return instance;
    }

    // container getter
    public ShuffledQuestionSet get(int id){
        return questionSets.get(id);
    }

    // private loader
    private void loadShuffledQuestions(){
        questionSets = new ShuffledQuestionsReader("toshuffle.json").loadQuestions();
    }
}
