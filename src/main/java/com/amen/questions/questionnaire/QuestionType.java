package com.amen.questions.questionnaire;

public enum QuestionType {
    SIMPLE_QUESTION,
    SHUFFLED_SET
}
