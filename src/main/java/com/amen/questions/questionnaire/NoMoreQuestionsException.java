package com.amen.questions.questionnaire;

public class NoMoreQuestionsException extends RuntimeException {
    public NoMoreQuestionsException(String message) {
        super(message);
    }
}
