package com.amen.questions.questionnaire;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShuffledQuestionsReader extends AbstractJsonReader{

    public ShuffledQuestionsReader(String fileName) {
        super(fileName);
    }

    public Map<Integer, ShuffledQuestionSet> loadQuestions() {
        Map<Integer, ShuffledQuestionSet> result = new HashMap<>();

        // read file, parse it into json and return value as JsonObject
        JsonObject fileContent = parseString(readFileContentIntoString()).getAsJsonObject("sets");

        Logger.getLogger(getClass().getName()).log(Level.INFO, "As JSONObject: " + fileContent);
        for (Map.Entry<String, JsonElement> entry : fileContent.entrySet()) {
            String key = entry.getKey(); // id pytania
            JsonArray value = entry.getValue().getAsJsonObject().getAsJsonArray("questions"); // pytanie

            try {
                // parse id
                Integer qId = Integer.parseInt(key);

                // parse question from json
                ShuffledQuestionSet question = new ShuffledQuestionSet(qId, value);

                // insert into map
                result.put(qId, question);
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            } catch (IllegalArgumentException iae) {
                iae.printStackTrace();
            }

        }

        return result;
    }
}