package com.amen.questions.questionnaire;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ShuffledQuestionSet {

    private int setId;
    private int initialSetSize;

    // if it isn't simple question load questions from shuffled questions to set
    private Set<Question> notYetAsked = new HashSet<>();
    private Set<Question> alreadyAsked = new HashSet<>();

    public ShuffledQuestionSet(int id, JsonArray jsonArray) {
        this.setId = id;

        this.initialSetSize = jsonArray.size();

        for (int i=0; i<jsonArray.size(); i++) {
            JsonObject object = jsonArray.get(i).getAsJsonObject();

            Question question = new Question(object);

            notYetAsked.add(question);
        }
    }

    public Question shuffleNext() throws NoMoreQuestionsException {
        Iterator<Question> questionIterator = notYetAsked.iterator();
        if (questionIterator.hasNext()) {
            Question question = questionIterator.next();

            notYetAsked.remove(question);

            alreadyAsked.add(question);

            return question;
        }

        throw new NoMoreQuestionsException("There are no more questions in this set.");
    }

    public boolean hasMoreQuestions() {
        return !notYetAsked.isEmpty();
    }
}
