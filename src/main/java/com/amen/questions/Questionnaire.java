package com.amen.questions;

import com.amen.questions.questionnaire.Question;
import com.amen.questions.questionnaire.QuestionsReader;
import com.amen.questions.questionnaire.ShuffleQuestionsContainer;

import java.util.*;

public class Questionnaire {
    private Queue<Integer> questionsOrder = new PriorityQueue<>();
    private Map<Integer, Question> questions;

    private Question currentQuestion = null;
    public Questionnaire() {
        questions = new QuestionsReader("pytania.json").loadQuestions();
        questionsOrder.addAll(questions.keySet());
    }

    public boolean hasQuestions() {
        return !questionsOrder.isEmpty();
    }

    public Question getNext() {
        if(currentQuestion != null &&
                currentQuestion.hasNext()){
            return currentQuestion.getNext();
        }
        currentQuestion = questions.get(questionsOrder.poll());
        if(currentQuestion.isSimple()) {
            return currentQuestion;
        }else{
            return currentQuestion.getNext();
        }
    }

}
